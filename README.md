﻿# the don't be shitty license

> magic is the tool of the oppressed
> - someone I went looking and didn't find it

# what is this?

licenses for code and other stuff work on two basic premises:

I went looking for the authorship of the phrase and just fucking forgot what I was gonna say

1. that the code and authors are somewhat protected by the law
2. that the law will be actually be used in favor of the code and the authors

both of then are... plain bullshit. maybe I'll expand later on why I think that now I'm tired

alas, just because the systems that are in place will not serve us in anyway it doesn't means that there shouldn't be consequences and repercussions.

enter the "don't be shitty license"

# okay fuck so what is this?
the "don't be shitty license" is a quite permissive license to protect the work of the oppressed and disenfranchised and what not. you can use it to grant permissions to people to use your work if they agree in being decent creatures and to suffer the consequences if they don't.

note that, this license has absolutely zero legal standing and shouldn't be used as such. also, if anybody ever gets fucking sued by someone else for using it please evoke bring to attention of whatever tribunal you're standing that someone is actually blaming an internet curse on any whatever that happened.

## how to use it
- slap the [license file](./LICENSE.md) in your project
	- remember to change the placeholder info to conform to your project
- add the servant's [sigil](./assets/shitty.png) on it
- make a prayer/charge the sigil invoking it to protect your stuff
- do whatever you feel like should be done to use it. it is magick after all

while I'll be using it primarily on code, you can use this on whatever you feel like. draw the sigil on stickers and slap it on stuff to keep shitty people away.

you can also add this as a secondary license to your project, just be sure to specify that on it and you're gucci.

## what is the sigil?
the sigil should be something simple to draw but distinct enough so you can recognize it.

in our case, it is a simple poop crossed over.

![the sigil in pixel art. it is kinda like a three with an extra leg, a little S over it and it is crossed over](./assets/shitty.png)

![several versions of the sigil drawn on a yellow stick note. they are a weird looking 3 thingy with an S over then and a line crossing them over](./assets/shitty_IRL.jpg)

you can steal the pixel art of it if you want to slap it somewhere as long as, you guessed, you don't be shitty

# contributing
if you want for whatever reason contribute for this license you are free to do so. you could do the git thingy of cloning the repo, then making a PR and whatever the fuck but that will be more of a hassle than anything else really. you can just contact me with suggestions over at the [itch.io](https://ardydo.itch.io/dbs-license) page and if I think it fits I'll add and credit you.

I'm particularly inclined to add a really absurdly long list of examples of "being shitty" but I really don't have the criativity or the time to come up with them by myself so if you wanna suggest any I'll gladly add it

also, if you do use the licence on any project feel free to also contact me so I can add it to the list/[itch collection](https://itch.io/c/2883356/dont-be-shitty-license-collection) of projects that use it!
